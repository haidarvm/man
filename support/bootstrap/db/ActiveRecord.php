<?php

namespace support\bootstrap\db;

use Webman\Bootstrap;
use ActiveRecord\ActiveDatabase;

class ActiveRecord implements Bootstrap {
    // 进程启动时调用
    public static function start($worker) {
        $db_config = [
            'dsn' => '',
            'hostname' => 'localhost',
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'database' => env('DB_DATABASE'),
            'dbdriver' => 'postgre',
            'pconnect' => false,
            'db_debug' => true
        ];
        $my_config = [
            'dsn' => '',
            'hostname' => 'localhost',
            'username' => 'root',
            'password' => env('DB_PASSWORD'),
            'database' => env('DB_DATABASE'),
            'dbdriver' => 'mysqli',
            'pconnect' => false,
            'db_debug' => true
        ];
        ActiveDatabase::addConfig('pg', $db_config);
        ActiveDatabase::addConfig('my', $my_config);
        //  ActiveDatabase::get('pg');
    }
}
