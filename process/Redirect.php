<?php
namespace process;

use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request;

class Redirect
{
    public function onMessage(TcpConnection $connection, Request $request)
    {
        $url = $request->host(true) . $request->uri();
        $connection->send(redirect("https://$url"));
    }
}