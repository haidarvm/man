<?php

namespace app\model;
use support\CI_Model;

class Posting extends CI_Model {

    public function randata() {
        $number = rand(1, 382745);
        $this->db->join('posts', 'posts.Id = postsid.post_id', 'inner');
        $query = $this->db->get_where('postsid', ['autoid' => $number]);
        $row = $query->row();
        return $row;
    }

    public function sing() {
        $query = $this->db->get_where('posts', ['Id' => 75987]);
        $row = $query->row();
        return $row;
    }
}
