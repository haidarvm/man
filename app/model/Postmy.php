<?php

namespace app\model;

use ActiveRecord\ActiveDatabase;

class Postmy {
    protected $db;

    public function __construct() {
        $this->db = ActiveDatabase::get('my');
    }

    public function randata() {
        $number = rand(1, 382745);
        $this->db->join('PostsId', 'Posts.Id = PostsId.post_id', 'inner');
        $query = $this->db->get_where('Posts', ['autoid' => $number]);
        $row = $query->row();
        return $row;
    }

    public function sing() {
        $query = $this->db->get_where('Posts', ['Id' => 75987]);
        $row = $query->row();
        return $row;
    }
}
