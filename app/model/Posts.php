<?php

namespace app\model;

use support\Model;

class Posts extends Model {

    protected $table = 'posts';
    protected $primaryKey = 'Id';

    public function postsid() {
        return $this->hasOne('app\model\Postsid');
    }

    public function joinPost($num) {
        return PostsId::join('posts', 'posts.Id', '=', 'postsid.post_id')->where('autoid', $num)->get();
    }
}
