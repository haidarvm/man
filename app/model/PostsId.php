<?php

namespace app\model;

use support\Model;

class Postsid extends Model {
    /**
     * 重定义主键，默认是id
     *
     * @var string
     */
    protected $table = 'postsid';
    protected $primaryKey = 'autoid';

    public function posts() {
        return $this->hasOne('app\model\Posts');
    }

    public function getDetail($num) {
        return $this->where('autoid', $num)->get();
    }
}
