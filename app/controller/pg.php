<?php

namespace app\controller;

use support\Request;
// use support\Db;
use app\model\Posts;
use app\model\PostsId;
use app\model\Posting;
use think\facade\Db;

class Pg {
    private $pquery;
    protected $posts;
    protected $posting;

    public function __construct() {
        $this->posts = new Posts();
        $this->posting = new Posting();
        // $this->pquery = $this->p_query()
    }

    public function index(Request $request) {
        $number = rand(1, 382745);
        $sql = 'SELECT * FROM "posts" p INNER JOIN "postsid" i ON i.post_id = p."Id"  where autoid =' . $number;
        $result = $this->p_query($sql);
        $result = pg_fetch_all($result);
        return json($result);
    }

    private function p_query($sql) {
        $db =env('DB_DATABASE');
        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $db_connection = pg_connect('host=localhost dbname='.$db.' user='.$user.' password='.$pass .'');
        $query = pg_query($db_connection, $sql);
        if (!$query) {
            echo "An error occurred.\n";
            return false;
        }
        return $query ;
    }

    public function sim(Request $request) {
        $number = rand(1, 382745);
        $post = PostsId::where('autoid', $number)->get();
        return json($post);
    }

    public function test(Request $request) {
        $num = rand(1, 382745);
        return json($this->posts->joinPost($num));
    }


    public function find(Request $request) {
        $post = Posts::find(15);
        return json($post);
    }

    public function think(Request $request) {
        $number = rand(1, 382745);
        $post = Db::table('posts')
        // ->join('postsid', 'posts.Id', '=', 'postsid.post_id')
        ->where('Id', $number)->get();
        return json($post);
    }

    public function active() {
        return json($this->posting->randata());
    }

    public function sing() {
        return json($this->posting->sing());
    }

    public function simple(Request $request) {
        $number = rand(1, 382745);
        $post = PostsId::join('posts', 'posts.Id', '=', 'postsid.post_id')->where('autoid', $number)->get();
        return json($post);
    }

    public function gen_text($file, $text) {
        $txt = $text;
        $full_path = public_path() . '/' . $file;
        file_put_contents($full_path, $txt . PHP_EOL, FILE_APPEND);
    }


    public function json(Request $request) {
        $hai = 'Hello haidar best';
        return json($hai);
    }
}
