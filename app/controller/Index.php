<?php

namespace app\controller;

use support\Request;
use support\Response;

class Index {
    public function index(Request $request) {
        return response('Hello World');
    }

    public function view(Request $request) {
        return view('index/view', ['name' => 'webman']);
    }

    public function json(Request $request) {
        return json(['code' => 0, 'msg' => 'ok']);
    }

    public function nginx(Request $request) {
        return view("index/nginx");
    }

    public function gzip(Request $request) {
        return response("Hello World");
        // return new Response(200, ['Content-Type' => 'application/json'], json_encode("haidar"));
    }

    public function file(Request $request) {
        $file = $request->file('upload');
        if ($file && $file->isValid()) {
            $file->move(public_path() . '/files/myfile.' . $file->getUploadExtension());
            return json(['code' => 0, 'msg' => 'upload success']);
        }
        return json(['code' => 1, 'msg' => 'file not found']);
    }
}
