<?php
/**
 * Here is your custom functions.
 */
function haidar($name) {
    return $name . 's';
}

function base_url() {
    $base_url = env('BASE_URL');
    return $base_url;
}

function imgFullPath() {
    $fullpath = 'assets/uploads/' . date('Y') . '/' . date('m') . '/';
    if (file_exists($fullpath)) {
        return $fullpath;
    }
    return  createImgPath();
}

function imgPath() {
    return date('Y') . '/' . date('m') . '/';
}

function imgPathUrl() {
    return base_url() . 'assets/uploads/' . date('Y') . '/' . date('m') . '/';
}

function imgBasePathUrl($img) {
    return base_url() . 'assets/uploads/' . $img;
}

function pathToUrl($fullpath) {
    $filename = getFileNameExt($fullpath);
    return imgPathUrl() . $filename;
}

function createImgPath() {
    $path = 'assets/uploads/';

    $year_folder = $path . date('Y');
    $month_folder = $year_folder . '/' . date('m');

    !file_exists($year_folder) && mkdir($year_folder, 0777);
    !file_exists($month_folder) && mkdir($month_folder, 0777);

    $path = $month_folder;
    return $path . $month_folder;
}

function saveNewFile($filecomplete, $prefix) {
    $newFile = getFileName($filecomplete) . '_' . $prefix . '.jpg';
    return  imgFullPath() . $newFile;
}

function uniqeID() {
    return base_convert(rand(1000000000, PHP_INT_MAX), 10, 36);
}


function getFileName($filecomplete) {
    return pathinfo($filecomplete, PATHINFO_FILENAME);
}

function getFileNameExt($filecomplete) {
    return pathinfo($filecomplete, PATHINFO_BASENAME);
}

function getFileExt($filecomplete) {
    return '.' . pathinfo($filecomplete, PATHINFO_EXTENSION);
}

function str_slug($string, $separator = '-') {
    // Convert all dashes/underscores into separator
    $flip = '-' == $separator ? '_' : '-';
    $string = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $string);

    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    $string = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', mb_strtolower($string));

    // Replace all separator characters and whitespace by a single separator
    $string = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $string);
    return trim($string, $separator);
}

function str_unslug($string, $separator = '-') {
    return ucwords(str_replace($separator, ' ', $string));
}

function rupiah($angka) {
    $hasil_rupiah = 'Rp ' . number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

function short_desc($desc) {
    return substr($desc, 0, 50);
}

function indoDate($date) {
    return date('d-m-Y', strtotime($date));
}

function indoEDate($date) {
    return date('d/m/Y', strtotime($date));
}

function sqlDate($date) {
    $dates = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($dates));
}

function indoDateTime($date) {
    return date('d-m-Y H:i', strtotime($date));
}

function getTimeOnly($datetime) {
    return date('H:i:s', strtotime($datetime));
}
