<?php
[
    'dsn' => '',
    'hostname' => 'localhost',
    'username' =>  env('DB_USERNAME'),
    'password' => env('DB_PASSWORD'),
    'database' => env('DB_DATABASE'),
    'dbdriver' => 'postgre',
    'pconnect' => false,
    'db_debug' => true
];