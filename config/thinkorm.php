<?php
return [
    'default'    =>    'pgsql',
    'connections'    =>    [
        'pgsql'    =>    [
            // 数据库类型
            'type'        => 'pgsql',
            // 服务器地址
            'hostname' => env('DB_HOST', 'localhost'),
            // 数据库名
            'database'    => env('DB_DATABASE', 'forge'),
            // 数据库用户名
            'username'    => env('DB_USERNAME', 'forge'),
            // 数据库密码
            'password'    => env('DB_PASSWORD', ''),

            // 'rw_separate' => true,
            // 数据库连接端口
            'hostport'    => '',
            // 数据库连接参数
            'params'      => [],
            // 数据库编码默认采用utf8
            'charset'     => 'utf8',
            // 数据库表前缀
            'prefix'      => '',
        ],
    ],
];